<?php
class Ape extends Animal
{
    public function __construct(
        public $name,
        public $legs = 2,
        public $cold_blooded = 'no'
    ) {
    }


    public function yell(): string
    {
        return 'Auooo';
    }


    /**
     * Get the value of legs
     */
    public function getLegs()
    {
        return $this->legs;
    }

    /**
     * Set the value of legs
     *
     * @return  self
     */
    public function setLegs($legs)
    {
        $this->legs = $legs;

        return $this;
    }
}
