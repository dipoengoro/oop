<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP</title>
</head>
<body>
    <?php
    require_once 'Animal.php';
    require_once 'Ape.php';
    require_once 'Frog.php';

    function toPrint($s1, $s2, $isBreak = false)
    {
        echo "$s1: $s2 <br>";
        if ($isBreak) {
            echo "<br>";
        }
    }

    $sheep = new Animal('Shaun');
    $sungokong = new Ape('Kera Sakti');
    $kodok = new Frog('Buduk');
    toPrint("Name", $sheep->getName());
    toPrint("Legs", $sheep->getLegs());
    toPrint("Cold Blooded", $sheep->getCold_blooded(), true);
    toPrint("Name", $kodok->getName());
    toPrint("Legs", $kodok->getLegs());
    toprint("Cold Blooded", $kodok->getCold_blooded());
    toprint("Jump", $kodok->jump(), true);
    toprint("Name", $sungokong->getName());
    toprint("Legs", $sungokong->getLegs());
    toprint("Cold Blooded", $sungokong->getCold_blooded());
    toprint("Yell", $sungokong->yell());
    ?>
</body>
</html>