<?php
class Animal
{
    public function __construct(
        public $name,
        public $legs = 4,
        public $cold_blooded = 'no'
    ) {
    }


    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of legs
     */
    public function getLegs()
    {
        return $this->legs;
    }

    /**
     * Set the value of legs
     *
     * @return  self
     */
    public function setLegs($legs)
    {
        $this->legs = $legs;

        return $this;
    }

    /**
     * Get the value of cold_blooded
     */
    public function getCold_blooded()
    {
        return $this->cold_blooded;
    }

    /**
     * Set the value of cold_blooded
     *
     * @return  self
     */
    public function setCold_blooded($cold_blooded)
    {
        $this->cold_blooded = $cold_blooded;

        return $this;
    }
}
?>